Sonata retail node code test
=======================

Hello, fellow javascripter!

If you are reading this you probably have thought about becoming part of our small but awesome team ... So good luck with the test!


Conditions
==========
There's no time limits to complete the test. We're not going to add any hidden code to monitor your first test server request to check when you start.  We're coders too and we have made code tests too, and we know how it works and how it feel. So take your time, seriously it's ok if you want to take a whole weekend to complete the test!


This code test is designed to take no more than a couple of hours, less for someone with good javascript skills. Basically we would like to know if you're able to produce simple and elegant code, how you test your code, if you're able to avoiding common problems and what's your code style (comments, structure, organization) and if you know and use the basic patterns.


Of course if you send us the solution after just 30 minutes we'll be impressed as hell and not so impressed if you answer in a week, but please keep in mind that we prefer a nice solution that took you a day than a dirty one that took you an hour. And since we know you're probably working somewhere else right now, if you just want to wait until the weekend or whenever you have time to do it, please, feel free! Just tell us "hey, I won't be able to look at this until saturday at least" and we'll be fine with it! :)


The problem
===========

We want you to build a slice of the API which supports the typical TODO webapp. You know, like the typical site or app where you can add notes, mark them as done, etc.  
  
We know is a hell of a work for a code test! Don't worry, we don't want you to implement the whole API, just a comple of entry points:  

--------------------------------------  
## TODO note management:  
You need to implement something like:  
> http://localhost:8080/todo/:id  

With the next actions:  

> - POST: Creates a new note in the database. It receives a json with note's title and text on the body of the request. :id paramenter is empty. The headers of the request must receive an userId and sessionId that you need to check for validity before saving the object to the db. You need to add the user info to the new note.  
> - PUT: Updates an existant note on the db. It receives a json with note's title & text on the body of the request, and the :id of an existant entry on the database. The headers of the request must receive an userId and sessionId that you need to check for validity before saving the object to the db. The user id should be the same than the user who created the note.   
> - DELETE: Deletes an existant note on the database. It receives an empty request's body and the :id of an existant entry on the database. The headers of the request must receive an userId and sessionId that you need to check for validity before saving the object to the db. The user id should be the same than the user who created the note.  
-----------------------------------  
## User notes view:  
You need to implement something like:
  
> http://localhost:8080/user/:id
  
With the next actions:  
> - GET: Search all the todo notes of the db and returns an array of the which belongs to the user with :id as id.
  
--------------------------  
We DON'T want you to implement login / session management (we are not so mean! :P). In your code, you can just mock the session management ... just add somewhere a asynchronous method / class / middleware (whatever you want) to check if a request has correct session data and make it return true 9 of each 10 requests.


What we want
============
- You need to build this using nodeJs (of course), express, mongoose and Q as promise library. You can also throw in some lo-dash / underscore if you want.
- Object oriented code
- Unit tests



